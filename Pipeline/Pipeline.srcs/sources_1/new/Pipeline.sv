import Common::*;

package Pipeline;

   typedef struct packed {
      Common::uint64_t current_pc;
      Common::raw_instr_t current_instruction;
      logic     is_bubble;
   } IF_ID_data_t;

   typedef struct packed {
      Common::control_out_t control_out;
      Common::int64_t signed_imm;
      Common::uint64_t read_data1;
      Common::uint64_t read_data2;
      Common::regId_t rs1;
      Common::regId_t rs2;
      Common::regId_t writeback_reg;
   } ID_EX_data_t;

   typedef struct packed {
      Common::writeback_control_t writeback_control;
      Common::mem_control_t mem_control;
      Common::uint64_t alu_result;
      Common::uint64_t write_data;
      Common::regId_t writeback_reg;
   } EX_MEM_data_t;

   typedef struct packed {
      Common::writeback_control_t writeback_control;
      Common::uint64_t mem_out;
      Common::uint64_t alu_result;
      Common::regId_t writeback_reg;
   } MEM_WB_data_t;

   typedef struct packed {
      logic       discard_instr_before_ex;
      logic       insert_bubble;
   } hazard_det_out_t;

   typedef struct packed {
      Common::uint64_t data1;
      Common::uint64_t data2;
   } forwarding_unit_out_t;

endpackage

module ForwardingUnit
  (output Pipeline::forwarding_unit_out_t out,
   input Common::uint64_t EX_reg_data1,
   input Common::uint64_t EX_reg_data2,
   input Common::regId_t EX_reg_rs1,
   input Common::regId_t EX_reg_rs2,
   input Common::regId_t MEM_writeback_reg,
   input Common::writeback_control_t MEM_writeback_control,
   input Common::uint64_t MEM_alu_result,
   input Common::regId_t WB_writeback_reg,
   input Common::writeback_control_t WB_writeback_control,
   input Common::uint64_t WB_writeback_result
   );

   always_comb begin
      if (MEM_writeback_reg != 0
          & MEM_writeback_control.reg_write
          & MEM_writeback_reg == EX_reg_rs1) begin
         assert(!MEM_writeback_control.mem_to_reg);
         out.data1 = MEM_alu_result;
      end else if (WB_writeback_reg != 0
                   & WB_writeback_control.reg_write
                   & WB_writeback_reg == EX_reg_rs1) begin
         out.data1 = WB_writeback_result;
      end else begin
         out.data1 = EX_reg_data1;
      end
   end

   always_comb begin
      if (MEM_writeback_reg != 0
          & MEM_writeback_control.reg_write
          & MEM_writeback_reg == EX_reg_rs2) begin
         assert(!MEM_writeback_control.mem_to_reg);
         out.data2 = MEM_alu_result;
      end else if (WB_writeback_reg != 0
                   & WB_writeback_control.reg_write
                   & WB_writeback_reg == EX_reg_rs2) begin
         out.data2 = WB_writeback_result;
      end else begin
         out.data2 = EX_reg_data2;
      end
   end

endmodule

module HazardDetUnit
  (output Pipeline::hazard_det_out_t out,
   input logic ID_is_branch,
   input logic EX_mem_read,
   input logic MEM_mem_read,
   input       Common::regId_t EX_writeback_reg,
   input       Common::regId_t MEM_writeback_reg,
   input       Common::regId_t ID_rs1,
   input       Common::regId_t ID_rs2
   );
always_comb begin
   out = '{default: 0};
   if ((EX_mem_read) & ((ID_rs1 == EX_writeback_reg & ID_rs1 != 0)
           | (ID_rs2 == EX_writeback_reg & ID_rs2 != 0))) begin
      out.discard_instr_before_ex = 1;
      if (ID_is_branch) begin
         out.insert_bubble = 1;
      end
   end else if (ID_is_branch & MEM_mem_read
                & ((ID_rs1 == MEM_writeback_reg & ID_rs1 != 0)
                 | (ID_rs2 == MEM_writeback_reg & ID_rs2 != 0)))
   begin
      out.discard_instr_before_ex = 1;
      //out.insert_bubble = 1;
   end
end
  endmodule
