import Common::*;

package Control;
   function Common::control_out_t control_from_instruction(Common::decoded_instr_t instr);
      automatic Common::control_out_t result = 0;
      Common::instr_e instr_name;
      
      if (instr.opcode == 0) begin
        return result;
      end
      
      instr_name = Common::get_instruction_name(instr);

      // is_branch
      result.is_branch = instr_name == Common::instr_beq;

      // mem_read
      unique case (instr_name)
        Common::instr_ld: result.mem_control.mem_read = 1;
        Common::instr_sd:result.mem_control.mem_read = 0;
        Common::instr_add:result.mem_control.mem_read = 0;
        Common::instr_sub:result.mem_control.mem_read = 0;
        Common::instr_and:result.mem_control.mem_read = 0;
        Common::instr_or:result.mem_control.mem_read = 0;
        Common::instr_beq:result.mem_control.mem_read = 0;
        default: result.mem_control.mem_read = 0;
      endcase

      // mem_write
      unique case (instr_name)
        Common::instr_ld: result.mem_control.mem_write = 0;
        Common::instr_sd:result.mem_control.mem_write = 1;
        Common::instr_add:result.mem_control.mem_write = 0;
        Common::instr_sub:result.mem_control.mem_write = 0;
        Common::instr_and:result.mem_control.mem_write = 0;
        Common::instr_or:result.mem_control.mem_write = 0;
        Common::instr_beq:result.mem_control.mem_write = 0;
        default: result.mem_control.mem_write = 0;
      endcase
      // mem_to_reg (if reg_write == 0, this does not matter)
      unique case (instr_name)
        Common::instr_ld: result.writeback_control.mem_to_reg = 1;
        Common::instr_sd:result.writeback_control.mem_to_reg = 'x;
        Common::instr_add:result.writeback_control.mem_to_reg = 0;
        Common::instr_sub:result.writeback_control.mem_to_reg = 0;
        Common::instr_and:result.writeback_control.mem_to_reg = 0;
        Common::instr_or:result.writeback_control.mem_to_reg = 0;
        Common::instr_beq:result.writeback_control.mem_to_reg = 'x;
        default: result.writeback_control.mem_to_reg = 0;
      endcase
      // alu_from_imm
      unique case (instr_name)
        Common::instr_ld: result.alu_control.alu_from_imm = 1;
        Common::instr_sd:result.alu_control.alu_from_imm = 1;
        Common::instr_add:result.alu_control.alu_from_imm = 0;
        Common::instr_sub:result.alu_control.alu_from_imm = 0;
        Common::instr_and:result.alu_control.alu_from_imm = 0;
        Common::instr_or:result.alu_control.alu_from_imm = 0;
        Common::instr_beq:result.alu_control.alu_from_imm = 0;
        default: result.alu_control.alu_from_imm = 0;
      endcase
      // alu_op
      unique case (instr_name)
        Common::instr_ld: result.alu_control.alu_op = Common::ALU_add;
        Common::instr_sd:result.alu_control.alu_op = Common::ALU_add;
        Common::instr_add:result.alu_control.alu_op = Common::ALU_add;
        Common::instr_sub:result.alu_control.alu_op = Common::ALU_sub;
        Common::instr_and:result.alu_control.alu_op = Common::ALU_AND;
        Common::instr_or:result.alu_control.alu_op = Common::ALU_OR;
        Common::instr_beq:result.alu_control.alu_op = Common::ALU_sub;
        default: result.alu_control.alu_op = Common::ALU_add;
      endcase
      // reg_write
      unique case (instr_name)
        Common::instr_ld: result.writeback_control.reg_write = 1;
        Common::instr_sd:result.writeback_control.reg_write = 0;
        Common::instr_add:result.writeback_control.reg_write = 1;
        Common::instr_sub:result.writeback_control.reg_write = 1;
        Common::instr_and:result.writeback_control.reg_write = 1;
        Common::instr_or:result.writeback_control.reg_write = 1;
        Common::instr_beq:result.writeback_control.reg_write = 0;
        default: result.writeback_control.reg_write = 0;
      endcase
      return result;
   endfunction
endpackage
