import Common::*;
import Control::*;
import Pipeline::*;


module PipelineCPU(
                   input logic clk,
                   input logic rst,
                   input       uint64_t in,
                   output      uint64_t out
                   );
   // Components & connection signals.
   Common::decoded_instr_t ID_decoded_instr = '{default:0};
   Common::int64_t ID_immediate_val = '{default:0};
   Common::control_out_t ID_control_out = '{default:0};
   logic                       ID_control_take_branch = 0;

   Pipeline::hazard_det_out_t hazard_out;
   Pipeline::forwarding_unit_out_t forwarding_unit_out;
   Pipeline::forwarding_unit_out_t branch_fwd_out;

   uint64_t alu_data1, alu_data2;
   uint64_t alu_result;
   Common::ALU_control_e alu_control;

   ALU alu(.control(alu_control),
           .data1(alu_data1),
           .data2(alu_data2),
           .result(alu_result));

   uint64_t reg_file_write_data;
   uint64_t reg_file_read_data1;
   uint64_t reg_file_read_data2;

   RegFile reg_file(.clk,
                    .rst,
                    .read_reg1(ID_decoded_instr.rs1),
                    .read_reg2(ID_decoded_instr.rs2),
                    .write_reg(MEM_WB_data_reg.writeback_reg),
                    .write_data(reg_file_write_data),
                    .write_enable(MEM_WB_data_reg.writeback_control.reg_write),
                    .read_data1(reg_file_read_data1),
                    .read_data2(reg_file_read_data2),
                    .reg30input(in),
                    .reg31output(out)
                    );

   uint64_t data_mem_out;
   DataMem data_mem(.clk,
                    .rst,
                    .mem_read(EX_MEM_data_reg.mem_control.mem_read),
                    .mem_write(EX_MEM_data_reg.mem_control.mem_write),
                    .address(EX_MEM_data_reg.alu_result),
                    .write_data(EX_MEM_data_reg.write_data),
                    .read_data(data_mem_out)
                    );

   // Pipeline registers
   IF_ID_data_t  IF_ID_data_reg;
   ID_EX_data_t  ID_EX_data_reg;
   EX_MEM_data_t EX_MEM_data_reg;
   MEM_WB_data_t MEM_WB_data_reg;

   IF_ID_data_t  IF_ID_data_next;
   ID_EX_data_t  ID_EX_data_next;
   EX_MEM_data_t EX_MEM_data_next;
   MEM_WB_data_t MEM_WB_data_next;

   // PC & instruction memory.
   uint64_t PC_reg = 0;
   uint64_t PC_next;

   // Connections Pipeline registers
   always_ff @(posedge clk) begin
      if (rst) begin
         IF_ID_data_reg <= '{default:0};
         ID_EX_data_reg <= '{default:0};
         EX_MEM_data_reg <= '{default:0};
         MEM_WB_data_reg <= '{default:0};
      end else begin
         IF_ID_data_reg  <= IF_ID_data_next;
         ID_EX_data_reg  <= ID_EX_data_next;
         EX_MEM_data_reg <= EX_MEM_data_next;
         MEM_WB_data_reg <= MEM_WB_data_next;
      end
   end

   Common::raw_instr_t instr_memory[256];
   initial $readmemh("/home/nicolas/Code/ACA/Pipeline/Pipeline.srcs/sources_1/new/program.hex", instr_memory);

   // Connections IF
   always_comb begin
      IF_ID_data_next.is_bubble = ID_control_take_branch | hazard_out.insert_bubble;
      if (!IF_ID_data_next.is_bubble) begin
         IF_ID_data_next.current_pc = PC_reg;
         IF_ID_data_next.current_instruction = instr_memory[PC_reg>>2];
      end else begin
         IF_ID_data_next.current_pc = IF_ID_data_reg.current_pc;
         IF_ID_data_next.current_instruction = IF_ID_data_reg.current_instruction;
      end
      
   end

   always_ff @(posedge clk) begin
      if (rst)
        PC_reg <= 0;
      else begin
         PC_reg <= PC_next;
      end
   end

   always_comb begin
      if (ID_control_take_branch)
        PC_next = IF_ID_data_reg.current_pc + ID_immediate_val;
      else if (IF_ID_data_next.is_bubble)
          PC_next = IF_ID_data_reg.current_pc;
      else
        PC_next = PC_reg + 4;
   end

   // Connections ID
  
   always_comb begin
      ID_decoded_instr = Common::decode_instruction(IF_ID_data_reg.current_instruction);
      ID_control_out = Control::control_from_instruction(ID_decoded_instr);
      
      ID_immediate_val = Common::sign_extend_imm(ID_decoded_instr);
      ID_control_take_branch = ID_control_out.is_branch 
                               & !hazard_out.discard_instr_before_ex
                               & !IF_ID_data_reg.is_bubble
                               & (branch_fwd_out.data1 == branch_fwd_out.data2);

      ID_EX_data_next.control_out = ID_control_out;
      ID_EX_data_next.read_data1 = reg_file_read_data1;
      ID_EX_data_next.read_data2 = reg_file_read_data2;
      ID_EX_data_next.signed_imm = ID_immediate_val;
      ID_EX_data_next.rs1 = ID_decoded_instr.rs1;
      ID_EX_data_next.rs2 = ID_decoded_instr.rs2;
      ID_EX_data_next.writeback_reg = ID_decoded_instr.rd;
      
      if (hazard_out.discard_instr_before_ex | IF_ID_data_reg.is_bubble) begin
         ID_EX_data_next = '{default: 0};
      end 
   end

   // Connections EX

   assign alu_control = ID_EX_data_reg.control_out.alu_control.alu_op;
   assign alu_data1 = forwarding_unit_out.data1;
   assign alu_data2 = ID_EX_data_reg.control_out.alu_control.alu_from_imm ?
                      ID_EX_data_reg.signed_imm : forwarding_unit_out.data2;

   always_comb begin
      EX_MEM_data_next.writeback_control = ID_EX_data_reg.control_out.writeback_control;
      EX_MEM_data_next.mem_control = ID_EX_data_reg.control_out.mem_control;
      EX_MEM_data_next.alu_result = alu_result;
      EX_MEM_data_next.write_data = forwarding_unit_out.data2;
      EX_MEM_data_next.writeback_reg = ID_EX_data_reg.writeback_reg;
   end

   // Connections MEM

   always_comb begin
      MEM_WB_data_next.writeback_control = EX_MEM_data_reg.writeback_control;
      MEM_WB_data_next.mem_out = data_mem_out;
      MEM_WB_data_next.alu_result = EX_MEM_data_reg.alu_result;
      MEM_WB_data_next.writeback_reg = EX_MEM_data_reg.writeback_reg;
   end

   // Connections WB.

   assign reg_file_write_data = MEM_WB_data_reg.writeback_control.mem_to_reg ?
                                MEM_WB_data_reg.mem_out :
                                MEM_WB_data_reg.alu_result;

   // Forwarding unit

   ForwardingUnit
     alu_forwarding(.out(forwarding_unit_out),
                .EX_reg_data1(ID_EX_data_reg.read_data1),
                .EX_reg_data2(ID_EX_data_reg.read_data2),
                .EX_reg_rs1(ID_EX_data_reg.rs1),
                .EX_reg_rs2(ID_EX_data_reg.rs2),
                .MEM_writeback_reg(EX_MEM_data_reg.writeback_reg),
                .MEM_writeback_control(EX_MEM_data_reg.writeback_control),
                .MEM_alu_result(EX_MEM_data_reg.alu_result),
                .WB_writeback_reg(MEM_WB_data_reg.writeback_reg),
                .WB_writeback_control(MEM_WB_data_reg.writeback_control),
                .WB_writeback_result(reg_file_write_data)
                );
                
    ForwardingUnit
      branch_forwarding(.out(branch_fwd_out),
                                .EX_reg_data1(reg_file_read_data1),
                                .EX_reg_data2(reg_file_read_data1),
                                .EX_reg_rs1(ID_decoded_instr.rs1),
                                .EX_reg_rs2(ID_decoded_instr.rs2),
                                .MEM_writeback_reg(EX_MEM_data_reg.writeback_reg),
                                .MEM_writeback_control(EX_MEM_data_reg.writeback_control),
                                .MEM_alu_result(EX_MEM_data_reg.alu_result),
                                .WB_writeback_reg(MEM_WB_data_reg.writeback_reg),
                                .WB_writeback_control(MEM_WB_data_reg.writeback_control),
                                .WB_writeback_result(reg_file_write_data)
                                );
   // Hazard det unit

   HazardDetUnit
     hazard(.out(hazard_out),
            .ID_is_branch(ID_control_out.is_branch),
            .EX_mem_read(ID_EX_data_reg.control_out.mem_control.mem_read),
            .MEM_mem_read(EX_MEM_data_reg.mem_control.mem_read),
            .EX_writeback_reg(ID_EX_data_reg.writeback_reg),
            .MEM_writeback_reg(EX_MEM_data_reg.writeback_reg),
            .ID_rs1(ID_decoded_instr.rs1),
            .ID_rs2(ID_decoded_instr.rs2));
endmodule
