module TOP(
    input logic clk,
    input logic sw,
    output logic[3:0] led
    );
    
    logic clk_lento;
    
    clk_10M clk_wiz(.clk_out1(clk_lento), .clk_in1(clk));
    
    PipelineCPU pipeline(.clk(clk_lento), .rst(sw), .in(0), .out(led));
    
endmodule
