`timescale 1 us / 100ns

import Common::*;

//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 08/19/2018 06:15:17 PM
// Design Name: 
// Module Name: TestBench
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module TestBench(

    );
    logic clk = 0;
    logic rst = 1;
    logic[3:0] led;
        
    always #1 clk = !clk;
    initial #2 rst = 0;
    PipelineCPU pipeline(.clk, .rst, .in(0), .out(led));
endmodule
