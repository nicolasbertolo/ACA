import Common::*;


module ALU
  (input ALU_control_t control,
   input        uint64_t data1,data2,
   output       uint64_t result,
   output logic is_zero
   );

   assign is_zero = result == 0;

   always_comb begin
      case (control)
        ALU_AND: begin
           result = data1 & data2;
        end
        ALU_OR: begin
           result = data1 | data2;
        end
        ALU_add: begin
           result = data1 + data2;
        end
        ALU_sub: begin
           result = data1 - data2;
        end
        default: begin
          result = 0; // avoid latch.
          assert(0) else $error("unknown ALU op");
        end
      endcase
   end

endmodule
