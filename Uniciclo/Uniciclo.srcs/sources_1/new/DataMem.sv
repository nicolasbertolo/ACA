import Common::*;

module DataMem(
 input logic clk, rst, mem_read, mem_write,
 input       uint64_t address, write_data,
 output      uint64_t read_data      );

   localparam N = 8;
   uint64_t mem [2**N-1];
   
   always_ff @(posedge clk) begin
      //if (rst) mem <= '{default:0};
      //else 
      if (mem_write) begin
         mem[address >> 2] <= write_data;
      end
   end

   assign read_data = mem_read ? mem[address>>2] : 0;
     
endmodule
