import Common::*;

module RegFile(
               input logic clk,
               input logic rst,
               input       regId_t read_reg1, read_reg2, write_reg,
               input       uint64_t write_data,
               input logic write_enable,
               output      uint64_t read_data1, read_data2,
               input       uint64_t reg30input,
               output      uint64_t reg31output);
   uint64_t registers[31];

   assign reg31output = registers[30];

   always_comb begin
      if (read_reg1 == 30) begin
         read_data1 = reg30input;
      end else if (read_reg1 == 31) begin
         read_data1 = registers[30];
      end else begin
         read_data1 = registers[read_reg1];
      end
   end

   always_comb begin
      if (read_reg2 == 30) begin
         read_data2 = reg30input;
      end else if (read_reg2 == 31) begin
         read_data2 = registers[30];
      end else begin
         read_data2 = registers[read_reg2];
      end
   end
//   initial begin
//        registers[1] = 1;
//   end

always_ff @(posedge clk) begin
   if (rst) begin
      registers  <= '{default:0};
      registers[1] <= 1;
   end else if (write_enable & write_reg != 0) begin
      if (write_reg == 31)
        registers[30] <= write_data;
      else
        if (write_reg < 30)
          registers[write_reg] <= write_data;
   end
end

endmodule // RegFile
