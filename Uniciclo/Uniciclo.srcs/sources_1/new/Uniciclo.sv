import Common::*;
import Control::*;

module Uniciclo(
                input logic clk,
                input logic rst,
                input       uint64_t in,
                output      uint64_t out
                );
   // Components & connection signals.
   Common::raw_instr_t instruction_reg;
   Common::decoded_instr_t decoded_instr;
   int64_t  immediate_val;

   Common::control_out_t control_out;
   uint64_t alu_data1, alu_data2;
   uint64_t alu_result;
   logic                    alu_is_zero;
   ALU alu(.control(control_out.alu_op),
           .data1(alu_data1),
           .data2(alu_data2),
           .result(alu_result),
           .is_zero(alu_is_zero));

   uint64_t reg_file_write_data;
   uint64_t reg_file_read_data1;
   uint64_t reg_file_read_data2;

   RegFile reg_file(.clk,
                    .rst,
                    .read_reg1(decoded_instr.rs1),
                    .read_reg2(decoded_instr.rs2),
                    .write_reg(decoded_instr.rd),
                    .write_data(reg_file_write_data),
                    .write_enable(control_out.reg_write),
                    .read_data1(reg_file_read_data1),
                    .read_data2(reg_file_read_data2),
                    .reg30input(in),
                    .reg31output(out)
                    );

   uint64_t data_mem_out;

   DataMem data_mem(.clk,
                    .rst,
                    .mem_read(control_out.mem_read),
                    .mem_write(control_out.mem_write),
                    .address(alu_result),
                    .write_data(reg_file_read_data2),
                    .read_data(data_mem_out)
                    );

   // PC & instruction memory.
   uint64_t PC_reg = 0;
   uint64_t PC_next;
   always_ff @(posedge clk) begin
      if (rst)
        PC_reg <= 0;
      else begin
        PC_reg <= PC_next;
//        $stop;
      end
   end

   always_comb begin
      if (control_out.is_branch & alu_is_zero)
        PC_next = PC_reg + immediate_val;
      else
        PC_next = PC_reg + 4;
   end

   Common::raw_instr_t instr_memory[256] = '{default:0};
   assign instruction_reg = instr_memory[PC_reg>>2];
   
   
   initial $readmemh("/home/nicolas/Code/ACA/Uniciclo/Uniciclo.srcs/sources_1/new/program.hex", instr_memory);

   // Instruction decoding.
   assign decoded_instr = Common::decode_instruction(instruction_reg);
   assign immediate_val = Common::sign_extend_imm(decoded_instr);

   // Control
   assign control_out = Control::control_from_instruction(decoded_instr);

   // Reg File

   assign reg_file_write_data = control_out.mem_to_reg ? data_mem_out : alu_result;

   // ALU
   
   assign alu_data1 = reg_file_read_data1;
   assign alu_data2 = control_out.alu_from_imm ? immediate_val : reg_file_read_data2;
endmodule
