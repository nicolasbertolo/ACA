`timescale 1ns / 1ps

import Common::*;

//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 08/19/2018 06:15:17 PM
// Design Name: 
// Module Name: TestBench
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module TestBench(

    );
    logic clk = 0;
    logic rst = 1;
    always #1 clk = !clk;
    initial #5 rst = 0;
    
    uint64_t in = 0;
    uint64_t out;
    
    Uniciclo uniciclo(.clk, .rst, .in, .out);
endmodule
