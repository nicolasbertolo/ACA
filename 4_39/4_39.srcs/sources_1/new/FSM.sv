`timescale 1ns / 1ps

module FSM(
input logic clk, rst, A, B,
output logic Z
    );
    
typedef struct packed
{
    logic An_1;
    logic An;
    logic Bn;
} state_t;

state_t state_reg;
state_t state_next;

const state_t initial_state = '{0,0,0};
    
always_ff @(posedge clk) begin
    if (rst) begin
        state_reg <= initial_state;
    end else begin
        state_reg <= state_next;
    end
end
    
always_comb begin
    state_next.An_1 = state_reg.An;
    state_next.An = A;
    state_next.Bn = B;
end

assign Z = state_reg.Bn ? (state_reg.An | state_reg.An_1) : (state_reg.An & state_reg.An_1); 
    
endmodule
