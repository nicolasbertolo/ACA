`timescale 1ns / 1ps

module PrioEnc
  #(parameter N = 8)
   (
    input logic [N-1:0]          data,
    output logic [$clog2(N)-1:0] out
    );

   logic                         isFirstHalf;

   assign isFirstHalf =| data[N-1:N/2];
   generate
      if (N > 2) begin
         logic [N/2-1:0] restData;
         assign restData = isFirstHalf ? data[N-1 : N/2] : data[N/2-1 : 0];
         PrioEnc #(N/2) rest(restData, out[$clog2(N)-2:0]);
         assign out[$clog2(N)-1] = isFirstHalf;
         //         logic [N/2-1:0] firstHalfData;
         //         logic [N/2-1:0]  lastHalfData;
         //         logic [$clog2(N)-1:0] outFirstHalf;
         //         logic [$clog2(N)-2:0] outLastHalf;

         //         assign firstHalfData = data[N-1 : N/2];
         //         assign  lastHalfData = data[N/2-1 : 0];
         
         //         PrioEnc #(N/2) (firstHalfData, outFirstHalf);
         //         PrioEnc #(N/2) (lastHalfData, outSecondHalf);

         //         assign out[$clog2(N)-1] = isFirstHalf;
         //         assign out[$clog2(N)-2:0] = isFirstHalf ? outFirstHalf : outSecondHalf;
      end
      else
        assign out = isFirstHalf;
   endgenerate
endmodule
