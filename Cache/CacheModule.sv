module CacheModule
  #(parameter BlockSize = 4, IndexBitSize = 10
    )
   (
    input logic  clk,
    input        uint64_t address,
    input        uint64_t write_data,
    input        write_enable,
    output logic data_ready,
    output       uint64_t read_data
    );
   typedef logic [63-IndexBitSize:0] tag_t;
   typedef uint64_t[BlockSize-1:0] block_t;

   typedef struct                    packed
                                     {
                                        logic
                                        logic valid1;
                                        tag_t tag1;
                                        block_t block1;
                                        logic valid2;
                                        tag_t tag2;
                                        block_t block2;
                                     } cache_line_t;

endmodule
