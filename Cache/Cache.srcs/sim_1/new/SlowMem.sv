`timescale 1ns / 1ps


module SlowMem
  #(parameter N = 6,
    parameter type word = byte,
    parameter DELAY = 100)
(
    input logic              clk, rst,
    input logic              r_enable, w_enable,
    input logic [N-1:0]      rawAddressIn,
    input                    word cpu_data_in,

    output logic             output_valid,
    output logic             ready_next,
    output                   word cpu_data_out
    );
    
    typedef struct {
       logic [N-1:0] addressIn;
       logic         r_enable;
       logic         w_enable;
       word          writeData;
    } op_data_t;
    
    word mem[2**N];
    int delayCounter;
    op_data_t op_data;
    
    
    always_ff @(posedge clk) begin
        if (rst) begin
            mem <= '{default: 0};
            op_data <= '{default: 0};
            delayCounter <= 0;
        end else begin
           if (delayCounter == 1 & op_data.w_enable) begin
                mem[op_data.addressIn] = op_data.writeData;
                op_data <= '{default: 0};
            end
            
            if (delayCounter == 0 & (r_enable | w_enable)) begin
                delayCounter <= DELAY;
                op_data.addressIn <= rawAddressIn;
                op_data.r_enable <= r_enable;
                op_data.w_enable <= w_enable;
                op_data.writeData <= cpu_data_in;
            end else if (delayCounter == 0) begin
                delayCounter <= 0;
                op_data <= '{default: 0};
            end else begin
                delayCounter <= delayCounter - 1;
            end
        end
    end
    
    always_comb begin
        ready_next = 0;
        output_valid = 0;
        cpu_data_out = 'x;
        if (delayCounter == 0) begin
            if (op_data.w_enable) begin
                cpu_data_out = op_data.writeData;
                output_valid = 1;
            end else if (op_data.r_enable) begin
                cpu_data_out = mem[op_data.addressIn];
                output_valid = 1;
            end 
        end
        if (delayCounter == 1 | 
            (delayCounter == 0 & !(r_enable | w_enable))) begin
            ready_next = 1;
        end
    end
    
endmodule
