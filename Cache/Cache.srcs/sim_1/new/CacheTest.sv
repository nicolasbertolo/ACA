`timescale 1ns / 1ps

module CacheTest(

                 );

   logic clk;
   logic rst;

   typedef logic[7:0] word;
   localparam t = 2;
   localparam k = 2;
   localparam b = 2;
   localparam N = t+k+b;

   logic        r_enable, w_enable;
   logic [N-1:0] address_from_cpu;
   word          data_from_cpu;
   logic         cache_output_valid;
   logic         cache_ready_next;
   logic         l2_output_valid;
   logic         l2_ready_next;
   logic         w_l2;
   logic         r_l2;
   logic [N-1:0] address_to_l2;
   word              data_from_l2;
   word              data_to_l2;
   word              data_to_cpu;

   SlowMem #(.N(N), .word(word), .DELAY(2))
   slow_mem(
            .clk, .rst,
            .r_enable(r_l2), .w_enable(w_l2),
            .rawAddressIn(address_to_l2),
            .cpu_data_in(data_to_l2),
            .output_valid(l2_output_valid),
            .ready_next(l2_ready_next),
            .cpu_data_out(data_from_l2)
            );

   CacheModule #(.t(t), .k(k), .b(b), .word(word))
   cache (
          .clk, .rst,
          .r_enable, .w_enable,
          .mem_ready_next(l2_ready_next),
          .mem_data_valid(l2_output_valid),
          .rawAddressIn(address_from_cpu),
          .cpu_data_in(data_from_cpu),
          .mem_data_in(data_from_l2),
          .ready_next(cache_ready_next),
          .output_valid(cache_output_valid),
          .w_l2,
          .r_l2,
          .memRawAddressOut(address_to_l2),
          .mem_data_out(data_to_l2),
          .cpu_data_out(data_to_cpu));


   initial begin
      rst = 1;
      clk = 1;
      repeat(4) #10 clk = ~clk;
      #10 rst = 0;
      clk = 0;
      forever #10  clk = ~clk;
   end
   
       initial begin
       r_enable = 0;
       w_enable = 0;
       @(negedge rst);
       r_enable = 0;
       w_enable = 1;
       address_from_cpu = 'h4;
       data_from_cpu = 25;
       //@(negedge cache_output_valid);
       @(posedge clk);
       r_enable = 0;
       w_enable = 0;
       address_from_cpu = 0;
       data_from_cpu = 0;
       
       #1;
       while (!cache_ready_next) @(posedge clk);
       r_enable = 0;
       w_enable = 1;
       address_from_cpu = 'h6;
       data_from_cpu = 26;
       @(posedge clk);
       #1;
       r_enable = 0;
       w_enable = 0;
       address_from_cpu = 0;
       data_from_cpu = 0;
       
       #1;
       while (!cache_ready_next) @(posedge clk);
       r_enable = 1;
       w_enable = 0;
       address_from_cpu = 'h4;
       data_from_cpu = 'x;
       @(posedge clk);
       r_enable = 0;
       w_enable = 0;
       address_from_cpu = 0;
       data_from_cpu = 0;
       @(posedge cache_output_valid);
       assert(data_to_cpu == 25);
       #30;
       
       #1;
       while (!cache_ready_next) @(posedge clk);
       r_enable = 1;
       w_enable = 0;
       address_from_cpu = 'b100100;
       data_from_cpu = 'x;
       @(posedge clk);
       r_enable = 0;
       w_enable = 0;
       address_from_cpu = 0;
       data_from_cpu = 0;
       @(posedge cache_output_valid);
       assert(data_to_cpu == 25);
       #30;
       
       while (!cache_ready_next) @(posedge clk);
       r_enable = 1;
       w_enable = 0;
       address_from_cpu = 'b110100;
       data_from_cpu = 'x;
       @(posedge clk);
       r_enable = 0;
       w_enable = 0;
       address_from_cpu = 0;
       data_from_cpu = 0;
       @(posedge cache_output_valid);
       assert(data_to_cpu == 25);
       #30;
       
       $finish;
   end

endmodule
