`timescale 1ns / 1ps

module SlowMemTest(
    );
    
    logic clk;
    logic rst;
    
    typedef byte word;
    localparam N = 6;
    
    logic              r_enable, w_enable;
    logic [N-1:0]      rawAddressIn;
    word cpu_data_in;

    logic             stall;
    word cpu_data_out;
    
    SlowMem #(.N(N), .word(word), .DELAY(5)) slow_mem(.*);
    
    initial begin
        rst = 1;
        clk = 1;
        repeat(4) #10 clk = ~clk;
        #10 rst = 0;
        clk = 0;
        forever #10  clk = ~clk;
    end
    
    initial begin
        @(negedge rst);
        r_enable = 0;
        w_enable = 1;
        rawAddressIn = 'h4;
        cpu_data_in = 25;
        assert(stall);
        @(posedge clk);
        #5;
        r_enable = 0;
        w_enable = 0;
        rawAddressIn = 0;
        cpu_data_in = 0;
        repeat(4) begin assert(stall); @(posedge clk); end
        assert(!stall);
        #30;
        r_enable = 0;
        w_enable = 1;
        rawAddressIn = 'h6;
        cpu_data_in = 26;
        assert(stall);
        @(posedge clk);
        #5;
        r_enable = 0;
        w_enable = 0;
        rawAddressIn = 0;
        cpu_data_in = 0;
        repeat(4) begin assert(stall); @(posedge clk); end
        assert(!stall);
        #30;
        r_enable = 1;
        w_enable = 0;
        rawAddressIn = 'h4;
        cpu_data_in = 'x;
        assert(stall);
        @(posedge clk);
        #5;
        r_enable = 0;
        w_enable = 0;
        rawAddressIn = 0;
        cpu_data_in = 0;
        repeat(4) begin assert(stall); @(posedge clk); end
        assert(!stall);
        assert(cpu_data_out == 25);
        $finish;
    end
endmodule
