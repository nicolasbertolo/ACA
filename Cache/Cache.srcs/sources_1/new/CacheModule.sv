typedef enum { cache_ready, cache_wait_lock, cache_wait_writeback, cache_read } cache_state_t;
typedef enum { lock_unlocked, write_lock, read_lock } lock_state_t;

module CacheModule
  #(parameter t = 4,
    parameter k = 2,
    parameter b = 2,
    parameter type word = logic[7:0])
   (
    input logic              clk, rst,
    input logic              r_enable, w_enable,
    input logic              mem_data_valid,
    input logic              mem_ready_next,
    input logic [t+k+b-1:0]  rawAddressIn,
    input                    word cpu_data_in,
    input                    word mem_data_in,

    output logic             output_valid,
    output logic             ready_next,
    output logic             w_l2,
    output logic             r_l2,
    output logic [t+k+b-1:0] memRawAddressOut,
    output                   word mem_data_out,
    output                   word cpu_data_out);

   parameter BLOCK_LENGTH = 2**b;
   parameter TABLE_LENGTH = 2**k;

   // Addresses
   typedef struct            packed
                             {
                                logic [t-1:0] tag;
                                logic [k-1:0] index;
                                logic [b-1:0] offset;
                             }address_t;

   address_t addressIn;
   address_t memAddressOut;

   assign addressIn = rawAddressIn;
   assign memRawAddressOut = {memAddressOut.tag, 
                              memAddressOut.index,
                              memAddressOut.offset};

   // Cache lines
   typedef struct                             packed {
      logic                                   valid1;
      logic                                   valid2;
      logic [BLOCK_LENGTH-1:0]                dirty1;
      logic [BLOCK_LENGTH-1:0]                dirty2;
      logic [t-1:0]                           tag1;
      logic [t-1:0]                           tag2;
      logic                                   mru1;
      word  [BLOCK_LENGTH-1:0] data1;
      word  [BLOCK_LENGTH-1:0] data2;
   } CacheLine_t;

   CacheLine_t cacheTable[TABLE_LENGTH];

   // Hit

   logic                                      hit1;
   logic                                      hit2;
   logic                                      hitAny;
   assign hitAny = hit1 | hit2;

   always_comb begin
      hit1 = 0;
      hit2 = 0;
      if (cacheTable[addressIn.index].valid1
          & cacheTable[addressIn.index].tag1 == addressIn.tag) begin
         hit1 = 1;
      end
      if (cacheTable[addressIn.index].valid2
          & cacheTable[addressIn.index].tag2 == addressIn.tag) begin
         hit2 = 1;
      end
   end

   // replacement
   logic canBeRetired1;
   logic canBeRetired2;
   logic needToRetire1;
   logic needToRetire2;

   assign canBeRetired1 = ! (|cacheTable[addressIn.index].dirty1);
   assign canBeRetired2 = ! (|cacheTable[addressIn.index].dirty2);

   always_comb begin
      needToRetire1 = 0;
      needToRetire2 = 0;
      
      if (cacheTable[addressIn.index].mru1) begin
         if (cacheTable[addressIn.index].tag2 != addressIn.tag) begin
            needToRetire2 = 1;
         end 
      end else begin
         if (cacheTable[addressIn.index].tag1 != addressIn.tag) begin
            needToRetire1 = 1;
         end
      end 
   end

   typedef struct packed {
      address_t     addressIn;
      logic       r_enable;
      logic       w_enable;
      logic       useWay1;
      word          writeData;
      logic [b-1:0] writeOffset;
   } op_data_t;

   (* fsm_encoding = "sequential" *)
   cache_state_t cache_state_reg;
   cache_state_t cache_state_next;

   address_t     writebackAddress_next;

   (* fsm_encoding = "sequential" *)
   lock_state_t lock_state_reg;
   lock_state_t lock_state_next;

   op_data_t cache_op_reg;
   op_data_t cache_op_next;
   
   CacheLine_t      OpDataCacheLine;
   logic            canBeRetired1OpData;
   logic            canBeRetired2OpData;
   assign OpDataCacheLine = cacheTable[cache_op_reg.addressIn.index];
   
   assign canBeRetired1OpData = ! (|OpDataCacheLine.dirty1);
   assign canBeRetired2OpData = ! (|OpDataCacheLine.dirty2);

   // lock management
   logic            want_read_lock_next;
   logic            want_write_lock_next;
   logic            grant_read_lock_next;
   logic            grant_write_lock_next;

   assign grant_read_lock_next = lock_state_next == read_lock;
   assign grant_write_lock_next = lock_state_next == write_lock;

   always_comb begin
      if (lock_state_reg == read_lock & want_read_lock_next) begin
         lock_state_next = read_lock;
      end else if (lock_state_reg == write_lock & want_write_lock_next) begin
         lock_state_next = write_lock;
      end else if (want_read_lock_next & mem_ready_reg) begin
         lock_state_next = read_lock;
      end else if (want_write_lock_next & mem_ready_reg) begin
         lock_state_next = write_lock;
      end else begin
         lock_state_next = lock_unlocked;
      end
   end

   logic mem_ready_reg;
   
   always_ff @(posedge clk) begin
      if (rst) begin
         cache_op_reg <= '{default: 'x};
         cache_state_reg <= cache_ready;
         lock_state_reg <= lock_unlocked;
         mem_ready_reg <= 1;
      end else begin
         mem_ready_reg <= mem_ready_next;
         cache_op_reg        <= cache_op_next;
         if (want_read_lock_next & !grant_read_lock_next) begin
            cache_state_reg <= cache_wait_lock;
         end else begin
            cache_state_reg     <= cache_state_next;
         end
         lock_state_reg      <= lock_state_next;
      end
   end
   
   // cache state
   always_comb begin
      cache_state_next = cache_ready;
      cache_op_next = '{default: 0};
      case (cache_state_reg)
        cache_ready: begin
           if (!hitAny & (r_enable | w_enable)) begin
              cache_op_next.addressIn = addressIn;
              cache_op_next.r_enable = r_enable;
              cache_op_next.w_enable = w_enable;
              cache_op_next.writeData = cpu_data_in;
              cache_op_next.writeOffset = 0;
              
              if (!cacheTable[addressIn.index].valid1) begin
                 cache_op_next.useWay1 = 1;
                 cache_state_next = cache_read;
              end else if (!cacheTable[addressIn.index].valid2) begin
                 cache_op_next.useWay1 = 0;
                 cache_state_next = cache_read;
              end
              if (needToRetire1) begin
                 cache_op_next.useWay1 = 1;
                 if (canBeRetired1) begin
                    cache_state_next = cache_read;
                 end else begin
                    cache_state_next = cache_wait_writeback;
                 end
              end else if (needToRetire2) begin
                 cache_op_next.useWay1 = 0;
                 if (canBeRetired2) begin
                    cache_state_next = cache_read;
                 end else begin
                    cache_state_next = cache_wait_writeback;
                 end
              end
           end else begin
              cache_state_next = cache_ready;
              cache_op_next = '{default: 0};
           end
        end
        cache_wait_writeback: begin
           cache_op_next = cache_op_reg;
           cache_state_next = cache_wait_writeback;
           if (( cache_op_reg.useWay1 & canBeRetired1OpData)
               | (!cache_op_reg.useWay1 & canBeRetired2OpData)) begin
              cache_state_next = cache_read;
           end
        end
        cache_read: begin
           cache_state_next = cache_read;
           cache_op_next = cache_op_reg;
           if (mem_ready_reg) begin
              if (cache_op_reg.writeOffset == BLOCK_LENGTH-1) begin
                 cache_state_next = cache_ready;
              end else begin
                 cache_op_next.writeOffset = cache_op_reg.writeOffset + 1;
              end
           end
        end
        cache_wait_lock: begin
           cache_state_next = cache_read;
           cache_op_next = cache_op_reg;
        end
        default: begin
           cache_state_next = cache_ready;
           cache_op_next = '{default: 0};
        end
      endcase

      want_read_lock_next = cache_state_next == cache_read | cache_state_next == cache_wait_lock;
   end

   assign ready_next = cache_state_next == cache_ready;

   always_comb begin
      want_write_lock_next = 0;
      mem_data_out = 0;
      writebackAddress_next = 0;
      
      //TODO
      if (cache_state_next == cache_wait_writeback) begin
         logic [BLOCK_LENGTH-1:0]                dirty;
         logic                                   found;

         writebackAddress_next = cache_op_next.addressIn;
         if (cache_op_next.useWay1) begin
            dirty = cacheTable[cache_op_next.addressIn.index].dirty1;
         end else begin
            dirty = cacheTable[cache_op_next.addressIn.index].dirty2;
         end

         found  = 0;
         for (int i = 0; i < 2**b; i = i + 1) begin
            if (dirty[i] && !found) begin
               writebackAddress_next.offset = i;
               found = 1;
            end
         end
      end

      if (writebackAddress_next != 0) begin
         want_write_lock_next = 1;
      end
      if (grant_write_lock_next) begin
         if (cacheTable[writebackAddress_next.index].mru1) begin
            mem_data_out = cacheTable[writebackAddress_next.index]
                           .data2[writebackAddress_next.offset];
         end else begin
            mem_data_out = cacheTable[writebackAddress_next.index]
                           .data1[writebackAddress_next.offset];
         end
      end
   end

   always_comb begin
      output_valid = 0;
      cpu_data_out = 'x;
      
      if (cache_state_reg == cache_ready) begin
         if (hit1) begin
            cpu_data_out = cacheTable[addressIn.index]
                           .data1[addressIn.offset];
            output_valid = 1;
         end else if (hit2) begin
            cpu_data_out = cacheTable[addressIn.index]
                           .data2[addressIn.offset];
            output_valid = 1;
         end
         if (!r_enable) begin
            cpu_data_out = 'x;
         end
      end else if (cache_state_reg == cache_read &
                   cache_op_reg.writeOffset == BLOCK_LENGTH-1 &
                   mem_data_valid) begin
         if (cache_op_reg.r_enable) begin
            if (cache_op_reg.addressIn.offset == BLOCK_LENGTH-1) begin
               cpu_data_out = mem_data_in;
            end else if (cache_op_reg.useWay1) begin
               cpu_data_out = cacheTable[cache_op_reg.addressIn.index]
                              .data1[cache_op_reg.addressIn.offset];
            end else begin
               cpu_data_out = cacheTable[cache_op_reg.addressIn.index]
                              .data2[cache_op_reg.addressIn.offset];
            end
         end else begin
            cpu_data_out = 0;
         end

         output_valid = 1;
      end
   end

   always_ff @(posedge clk) begin
      if (rst) begin
         cacheTable <= '{default: 0};
      end else begin
         if (grant_write_lock_next) begin
            if (cacheTable[writebackAddress_next.index].mru1) begin
               cacheTable[writebackAddress_next.index].dirty2[writebackAddress_next.offset] <= 0;
            end else begin
               cacheTable[writebackAddress_next.index].dirty1[writebackAddress_next.offset] <= 0;
            end
         end
         case (cache_state_reg)
           cache_ready: begin
              if ((r_enable & output_valid) | w_enable) begin
                 cacheTable[addressIn.index].mru1 <= hit1;
              end
              if (w_enable) begin
                 if (hit1) begin
                    cacheTable[addressIn.index].mru1 <= 1;
                    cacheTable[addressIn.index].data1[addressIn.offset] <= cpu_data_in;
                    cacheTable[addressIn.index].dirty1[addressIn.offset] <= 1;
                 end else if (hit2) begin
                    cacheTable[addressIn.index].mru1 <= 0;
                    cacheTable[addressIn.index].data2[addressIn.offset] <= cpu_data_in;
                    cacheTable[addressIn.index].dirty2[addressIn.offset] <= 1;
                 end
              end
           end
           cache_read: begin
              if (mem_data_valid) begin
                 if (cache_op_reg.useWay1) begin
                    if (cache_op_reg.w_enable & cache_op_reg.addressIn.offset == cache_op_reg.writeOffset) begin
                       cacheTable[cache_op_reg.addressIn.index]
                         .data1[cache_op_reg.writeOffset] <= cache_op_reg.writeData;
                    end else begin
                       cacheTable[cache_op_reg.addressIn.index]
                         .data1[cache_op_reg.writeOffset] <= mem_data_in;
                    end
                 end else begin
                    if (cache_op_reg.w_enable & cache_op_reg.addressIn.offset == cache_op_reg.writeOffset) begin
                       cacheTable[cache_op_reg.addressIn.index]
                         .data2[cache_op_reg.writeOffset] <= cache_op_reg.writeData;
                    end else begin
                       cacheTable[cache_op_reg.addressIn.index]
                         .data2[cache_op_reg.writeOffset] <= mem_data_in;
                    end
                 end
                 if (cache_op_reg.writeOffset == BLOCK_LENGTH-1) begin
                    if (cache_op_reg.useWay1) begin
                       cacheTable[cache_op_reg.addressIn.index].mru1 <= 1;
                       cacheTable[cache_op_reg.addressIn.index].valid1 <= 1;
                       cacheTable[cache_op_reg.addressIn.index].tag1 <= cache_op_reg.addressIn.tag;
                    end else begin
                       cacheTable[cache_op_reg.addressIn.index].mru1 <= 0;
                       cacheTable[cache_op_reg.addressIn.index].valid2 <= 1;
                       cacheTable[cache_op_reg.addressIn.index].tag2 <= cache_op_reg.addressIn.tag;
                    end
                 end
              end
           end
           default: begin
           end
         endcase
      end
   end

   always_comb begin
      memAddressOut = '{default: 'x};
      r_l2 = 0;
      w_l2 = 0;
      if (mem_ready_reg) begin
         if (grant_write_lock_next) begin
            memAddressOut = writebackAddress_next;
            w_l2 = 1;
         end else if (grant_read_lock_next) begin
            memAddressOut = cache_op_next.addressIn;
            memAddressOut.offset = cache_op_next.writeOffset;
            r_l2 = 1;
         end
      end
   end
endmodule
